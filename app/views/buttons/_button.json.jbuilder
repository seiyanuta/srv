json.extract! button, :id, :name, :count, :created_at, :updated_at
json.url button_url(button, format: :json)