Rails.application.routes.draw do
  resources :buttons
  post "/webhook", to: "buttons#webhook"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
